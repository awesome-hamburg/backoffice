package org.szhao.backoffice.controller

import org.springframework.http.ResponseEntity
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.GetMapping

@Controller
class UserController {
    @GetMapping("/hi")
    fun getUsers(): ResponseEntity<User> {
        return ResponseEntity.ok(User("ciao"))
    }
}

data class User(
    val username: String
)
